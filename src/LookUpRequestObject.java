import java.io.Serializable;
import java.util.ArrayList;

public class LookUpRequestObject implements Serializable{
    private Node source;
    private int noOfHopsTaken;
    private int idealDestination;
    private int dataToBeStored;
    private int noOfHopsRequired;


    LookUpRequestObject(Node source, int noOfHopsTaken, int idealDestination, int dataToBeStored,int noOfHopsRequired){
        this.source = source;
        this.idealDestination = idealDestination;
        this.noOfHopsTaken = noOfHopsTaken;
        this.dataToBeStored = dataToBeStored;
        this.noOfHopsRequired = noOfHopsRequired;
    }
    public int getNoOfHopsTaken() {
        return noOfHopsTaken;
    }

    public void setNoOfHopsTaken(int noOfHopsTaken) {
        this.noOfHopsTaken = noOfHopsTaken;
    }

    public int getIdealDestination() {
        return idealDestination;
    }

    public void setIdealDestination(int idealDestination) {
        this.idealDestination = idealDestination;
    }

    public Node getSource() {
        return source;
    }

    public void setSource(Node source) {
        this.source = source;
    }

    public int getDataToBeStored() {
        return dataToBeStored;
    }

    public void setDataToBeStored(int dataToBeStored) {
        this.dataToBeStored = dataToBeStored;
    }

    public int getNoOfHopsRequired() {
        return noOfHopsRequired;
    }

    public void setNoOfHopsRequired(int noOfHopsRequired) {
        this.noOfHopsRequired = noOfHopsRequired;
    }
}
