import java.net.MalformedURLException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ServerInterface extends Remote {
    Node connectToServer(Node node) throws RemoteException, MalformedURLException;
    void removeFromChordNetwork(Node node) throws RemoteException;
}
