import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;

public class Node implements Serializable{
        private int myID;
        private String myIP;
        private int myPort;
        private boolean isOnline;
        private HashMap<Integer, Node> fingerTable ;
        private int originalID;
        private ArrayList<Integer> dataStorage;

    public void setID (int id) {
            this.myID = id;
        }
        public void setIP (String ip) {
            this.myIP = ip;
        }
        public void setPort (int port) {
            this.myPort = port;
        }
        public int getID() {
            return this.myID;
        }
        public String getIP() {
            return this.myIP;
        }
        public int getPort() {
            return this.myPort;
        }

        public boolean getOnlineStatus(){
           return isOnline;
        }

        public void setOnlineStatus(){
            this.isOnline = isOnline;
        }

        public HashMap<Integer, Node> getFingerTable() {
            return fingerTable;
        }

        public void setFingerTable(HashMap<Integer, Node> fingerTable) {
            this.fingerTable = fingerTable;
        }

        Node(int id, String ip, int port, boolean isOnline, HashMap<Integer, Node> fingerTable, int originalID, ArrayList<Integer> dataStorage){
            this.myID = id;
            this.myIP = ip;
            this.myPort = port;
            this.isOnline = isOnline;
            this.fingerTable = fingerTable;
            this.originalID = originalID;
            this.dataStorage = dataStorage;
        }


    public int getOriginalID() {
        return originalID;
    }

    public void setOriginalID(int originalID) {
        this.originalID = originalID;
    }

    public ArrayList<Integer> getDataStorage() {
        return dataStorage;
    }

    public void setDataStorage(ArrayList<Integer> dataStorage) {
        this.dataStorage = dataStorage;
    }
}
