import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.*;

public class ChordNode extends UnicastRemoteObject implements NodeInterface {
    private static Node myNode;

    private ChordNode() throws RemoteException {
    }

    public static void main(String args[]) throws RemoteException, NotBoundException {
        ChordNode cn = new ChordNode();
        System.out.println("Enter your Node ID");
        Scanner sc = new Scanner(System.in);
        int myID = sc.nextInt();
        myNode = new Node(myID % 16, "127.0.0.1", 50086, true, new HashMap<>(), myID, new ArrayList<>());
        try {
            createRegistryForServerConnection(myID, cn);
            callRegistrationServer(myNode);

        } catch (RemoteException | NotBoundException | MalformedURLException e) {
            e.printStackTrace();
        }
        //System.out.println("Hopefully yeh correct hai");
        //printFingerTable();
        boolean done = false;
        while (!done) {
            System.out.println("Select 1 of these tasks that you want to do:");
            System.out.println("Press 1 for displaying the finger table of Node : " + myNode.getID() + "\n"
                    + "Press 2 to upload data into chord zone \n"
                    + "Press 3 to download file from chord zone \n"
                    + "Press 4 for quitting and leaving the chord zone \n"
            + "Press 5 for showing all the files stored on this node\n");

            while (sc.hasNextInt()) {
                switch (sc.nextInt()) {
                    case 1:
                        printFingerTable();
                        break;
                    case 2:
                        System.out.println("Enter the data in integer format that you want to store on the chord network : ");
                        cn.upload();
                        break;
                    case 3:
                        System.out.println("Enter the data in integer format that you want to look for on the chord network : ");
                        cn.download();
                        break;
                    case 4:
                        leaveChordZone();
                        done = true;
                        System.exit(0);
                    case 5:
                        System.out.println("These are the files stored on this node at this moment : ");
                        System.out.println(myNode.getDataStorage());
                        break;
                    default:
                        System.out.println("Incorrect input. Please try again...");
                        done = false;
                        break;
                }
            }
        }
    }

    private void download() throws RemoteException, NotBoundException {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int lookUpData = scanner.nextInt();
            int idealDestination = lookUpData % 16;
            LookUpRequestObject lookUpRequestObject = new LookUpRequestObject(myNode, 0, idealDestination, lookUpData,0);
            this.goToDestinationForLookUp(lookUpRequestObject);
        }
    }

    private void goToDestinationForLookUp(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException {
        int currentNodePos = lookUpRequestObject.getSource().getID();
        if (lookUpRequestObject.getIdealDestination() < currentNodePos) {
            lookUpRequestObject.setNoOfHopsRequired((int) (Math.pow(2, 4) - currentNodePos + lookUpRequestObject.getIdealDestination()));
            this.routeForLookUp(lookUpRequestObject);
        } else if (lookUpRequestObject.getIdealDestination() > currentNodePos) {
            lookUpRequestObject.setNoOfHopsRequired(lookUpRequestObject.getIdealDestination() - currentNodePos);
            this.routeForLookUp(lookUpRequestObject);
        } else {
            if(myNode.getDataStorage().contains(lookUpRequestObject.getDataToBeStored())){
                System.out.println("Data :"+ lookUpRequestObject.getDataToBeStored() + " found on Node : "+myNode.getID());
            }
        }
    }

    public void routeForLookUp(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException {
        if (lookUpRequestObject.getNoOfHopsRequired() > 0) {
            int jumpTo = -1;
            for (Integer fingers : myNode.getFingerTable().keySet()) {
                if (lookUpRequestObject.getNoOfHopsRequired() > fingers) {
                    if (jumpTo == -1) {
                        jumpTo = fingers;
                    } else if (jumpTo < fingers) {
                        jumpTo = fingers;
                    }
                }
                if (jumpTo == -1) {
                    jumpTo = findMinKey(myNode.getFingerTable());
                }
            }
            int hopsToBeTaken;
            if (myNode.getFingerTable().get(jumpTo).getID() > lookUpRequestObject.getSource().getID()) {
                hopsToBeTaken = myNode.getFingerTable().get(jumpTo).getID() - lookUpRequestObject.getSource().getID();
            } else {
                hopsToBeTaken = (int) (Math.pow(2, 4) - lookUpRequestObject.getSource().getID() + myNode.getFingerTable().get(jumpTo).getID());
            }
            lookUpRequestObject.setNoOfHopsRequired(lookUpRequestObject.getNoOfHopsRequired() - hopsToBeTaken);
            lookUpRequestObject.setNoOfHopsTaken(Math.abs(lookUpRequestObject.getNoOfHopsTaken() + hopsToBeTaken));
            Registry myReg = LocateRegistry.getRegistry(myNode.getFingerTable().get(jumpTo).getIP(), 50086 + myNode.getFingerTable().get(jumpTo).getID());
            NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + myNode.getFingerTable().get(jumpTo).getID());
            nodeInterface.routeForLookUp(lookUpRequestObject);

            //route it further by manipulating the lookUpRequestObject
        } else {
            if (myNode.getDataStorage().contains(lookUpRequestObject.getDataToBeStored())) {
                System.out.println("Data " + lookUpRequestObject.getDataToBeStored() + " is being requested by Node : " + lookUpRequestObject.getSource().getID());
                Registry myReg = LocateRegistry.getRegistry(lookUpRequestObject.getSource().getIP(), 50086 + lookUpRequestObject.getSource().getID());
                NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + lookUpRequestObject.getSource().getID());
                nodeInterface.sendNotificationForLookUp(lookUpRequestObject, myNode.getID(), true);
            }else{
                Registry myReg = LocateRegistry.getRegistry(lookUpRequestObject.getSource().getIP(), 50086 + lookUpRequestObject.getSource().getID());
                NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + lookUpRequestObject.getSource().getID());
                nodeInterface.sendNotificationForLookUp(lookUpRequestObject, myNode.getID(), false);
            }
        }
    }

    public void sendNotificationForLookUp(LookUpRequestObject lookUpRequestObject, int requestID, boolean foundData){
        if(foundData){
            System.out.println("Data : "+ lookUpRequestObject.getDataToBeStored() + " found in the chord system on Node : "+requestID);
        }else{
            System.out.println("Your data is not currently stored on the chord system");
        }
    }

    private void upload() throws RemoteException, NotBoundException {
        Scanner scanner = new Scanner(System.in);
        if (scanner.hasNextInt()) {
            int data = scanner.nextInt();
            int idealDestination = data % 16;
            LookUpRequestObject lookUpRequestObject = new LookUpRequestObject(myNode, 0, idealDestination, data,0);
            this.routeDataToDestination(lookUpRequestObject);
        }
    }

    public void changeFileLocations(int data) throws RemoteException, NotBoundException{
        int idealDestination = data % 16;
        LookUpRequestObject lookUpRequestObject = new LookUpRequestObject(myNode, 0, idealDestination, data,0);
        this.routeDataToDestination(lookUpRequestObject);

    }

    @Override
    public void updateFileLocationsInAllNodes() throws RemoteException, NotBoundException {
        if(!myNode.getDataStorage().isEmpty()){
            for(Integer filesOnThisNode : myNode.getDataStorage()){
                changeFileLocations(filesOnThisNode);
                System.out.println("Changed File Location of File : " + filesOnThisNode);
            }
            System.out.println("Done with updating all files locations on this node ...");
        }
    }

    private void routeDataToDestination(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException {
        int currentNodePos = lookUpRequestObject.getSource().getID();
        if (lookUpRequestObject.getIdealDestination() < currentNodePos) {
            lookUpRequestObject.setNoOfHopsRequired((int) (Math.pow(2, 4) - currentNodePos + lookUpRequestObject.getIdealDestination()));
            this.route(lookUpRequestObject);
        } else if (lookUpRequestObject.getIdealDestination() > currentNodePos) {
            lookUpRequestObject.setNoOfHopsRequired(lookUpRequestObject.getIdealDestination() - currentNodePos);
            this.route(lookUpRequestObject);
        } else {
            myNode.getDataStorage().add(lookUpRequestObject.getDataToBeStored());
            System.out.println("Data "+ lookUpRequestObject.getDataToBeStored() + " stored on node : "+myNode.getID());
        }
    }

    public void route(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException {
        if (lookUpRequestObject.getNoOfHopsRequired() > 0) {
            int jumpTo = -1;
            for (Integer fingers : myNode.getFingerTable().keySet()) {
                if(lookUpRequestObject.getNoOfHopsRequired() == fingers){
                    jumpTo = fingers;
                    break;
                }
                if (lookUpRequestObject.getNoOfHopsRequired() > fingers) {
                    if (jumpTo == -1) {
                        jumpTo = fingers;
                    } else if (jumpTo < fingers) {
                        jumpTo = fingers;
                    }
                }
                if (jumpTo == -1) {
                    jumpTo = findMinKey(myNode.getFingerTable());
                }
            }
            int hopsToBeTaken;
            if(myNode.getFingerTable().get(jumpTo).getID() > lookUpRequestObject.getSource().getID()){
                hopsToBeTaken = myNode.getFingerTable().get(jumpTo).getID() - lookUpRequestObject.getSource().getID();
            }else{
                hopsToBeTaken = (int) (Math.pow(2, 4) - lookUpRequestObject.getSource().getID() + myNode.getFingerTable().get(jumpTo).getID());
            }
            lookUpRequestObject.setNoOfHopsRequired(lookUpRequestObject.getNoOfHopsRequired() - hopsToBeTaken);
            lookUpRequestObject.setNoOfHopsTaken(Math.abs(lookUpRequestObject.getNoOfHopsTaken() + hopsToBeTaken));
            Registry myReg = LocateRegistry.getRegistry(myNode.getFingerTable().get(jumpTo).getIP(), 50086 + myNode.getFingerTable().get(jumpTo).getID());
            NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node"+myNode.getFingerTable().get(jumpTo).getID());
            nodeInterface.route(lookUpRequestObject);

            //route it further by manipulating the lookUpRequestObject
        } else {
            myNode.getDataStorage().add(lookUpRequestObject.getDataToBeStored());
            System.out.println("Data "+ lookUpRequestObject.getDataToBeStored() + " stored on node : "+myNode.getID());
            Registry myReg = LocateRegistry.getRegistry(lookUpRequestObject.getSource().getIP(), 50086 + lookUpRequestObject.getSource().getID());
            NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node"+lookUpRequestObject.getSource().getID());
            nodeInterface.sendNotification(lookUpRequestObject, myNode.getID());
        }
    }

    public void sendNotification(LookUpRequestObject lookUpRequestObject, int id) throws RemoteException{
        System.out.println("Your data : " + lookUpRequestObject.getDataToBeStored() + " was successfully stored on the chord system on Node : "+id);
    }

    private int findMinKey(HashMap<Integer, Node> fingerTable) {
        int smallest = 16;
        for (Integer fingers : fingerTable.keySet()) {
            if (fingers < smallest)
                smallest = fingers;
        }
        return smallest;
    }

    private static void leaveChordZone() throws RemoteException, NotBoundException {
        Registry myReg = LocateRegistry.getRegistry(myNode.getIP(), myNode.getPort());
        ServerInterface serverInterface = (ServerInterface) myReg.lookup("server");
        System.out.println("Deleting Node from chord network and updating al the files and finger tables accordingly ... ");
        serverInterface.removeFromChordNetwork(myNode);

    }

    private static void createRegistryForServerConnection(int nodeID, ChordNode cn) throws RemoteException {
        Registry reg = LocateRegistry.createRegistry(50086 + nodeID);
        reg.rebind("node" + nodeID, cn); // giving name to the registry
        System.out.println("Node " + nodeID + " created the registry for the server to connect ... ");
    }


    private static void callRegistrationServer(Node node) throws RemoteException, NotBoundException, MalformedURLException {
        Registry myReg = LocateRegistry.getRegistry(node.getIP(), node.getPort());
        ServerInterface serverInterface = (ServerInterface) myReg.lookup("server");
        Node updatedNode = serverInterface.connectToServer(node);
        myNode.setFingerTable(updatedNode.getFingerTable());
        printFingerTable();
    }

    private static void printFingerTable() {
        System.out.println("Finger Table for node:" + myNode.getID());
        for (Map.Entry<Integer, Node> entry : myNode.getFingerTable().entrySet()) {
            System.out.println("key : " + entry.getKey() + " Value : " + entry.getValue().getID());
        }
    }

    @Override
    public void updateFingerTable(Node updatedNode) {
        System.out.println("Updated the Finger Table for Node " + updatedNode.getID());
        myNode.setFingerTable(updatedNode.getFingerTable());
        printFingerTable();
    }
}
