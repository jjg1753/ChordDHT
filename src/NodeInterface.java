import java.rmi.NotBoundException;
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface NodeInterface extends Remote{
    void updateFingerTable(Node updatedNode) throws RemoteException;
    void route(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException;
    void sendNotification(LookUpRequestObject lookUpRequestObject, int id) throws RemoteException;
    void routeForLookUp(LookUpRequestObject lookUpRequestObject) throws RemoteException, NotBoundException;

    void sendNotificationForLookUp(LookUpRequestObject lookUpRequestObject, int id, boolean foundData)throws RemoteException;
    void changeFileLocations(int data) throws RemoteException, NotBoundException;
    void updateFileLocationsInAllNodes() throws RemoteException, NotBoundException;
}
