
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.HashMap;

public class ServerInterfaceImplementation extends UnicastRemoteObject implements ServerInterface{
    private HashMap<Integer, Node> nodeHashMap = new HashMap<>();
    private int[] listOfNodesOnline = new int[16];

    ServerInterfaceImplementation() throws RemoteException {
    }

    @Override
    public Node connectToServer(Node node) {
        return(addNodeToHashMap(node));
    }

    @Override
    public void removeFromChordNetwork(Node deleteNode) throws RemoteException {
        System.out.println("Node : "+deleteNode.getID() +" being removed from the Chord System");
        nodeHashMap.remove(deleteNode.getID());
        listOfNodesOnline[deleteNode.getID()] = 0;
        updateAllNodesFingerTables(deleteNode);
        System.out.println("Finger tables updated after removing Node : "+deleteNode.getID());
        updateAllFileLocations(deleteNode);
    }

    private void updateAllFileLocations(Node deleteNode) {
        Node nodeToBeUsedToUpdateFiles = null;
        for (int i = 0; i < listOfNodesOnline.length; i++) {
            if (listOfNodesOnline[i] == 1) {
                nodeToBeUsedToUpdateFiles = nodeHashMap.get(i);
                break;
            }
        }
        Registry myReg;
        if (!deleteNode.getDataStorage().isEmpty()) {
            for (Integer files : deleteNode.getDataStorage()) {
                try {
                    myReg = LocateRegistry.getRegistry(nodeToBeUsedToUpdateFiles.getIP(), 50086 + nodeToBeUsedToUpdateFiles.getID());
                    NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + nodeToBeUsedToUpdateFiles.getID());
                    nodeInterface.changeFileLocations(files);
                } catch (RemoteException | NotBoundException e) {
                    e.printStackTrace();
                } catch (NullPointerException e) {
                    System.out.println("OOPS! No nodes left on the chord system and therefore this exception : " + e);
                }
            }
        } else {
            updateAllFilesOnAllNodesExceptForDeleted();
        }
    }

    private void updateAllFilesOnAllNodesExceptForDeleted() {
        Registry myReg;
        for (Node liveNodes : nodeHashMap.values()) {
            try {
                myReg = LocateRegistry.getRegistry(liveNodes.getIP(), 50086 + liveNodes.getID());
                NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + liveNodes.getID());
                nodeInterface.updateFileLocationsInAllNodes();
            } catch (RemoteException | NotBoundException e) {
                e.printStackTrace();
            }
        }
    }

    private Node addNodeToHashMap(Node node) {
        nodeHashMap.put(node.getID(), node);
        listOfNodesOnline[node.getID()] = 1;
        node = createFingerTable(node);
        System.out.println("Node connected. Node ID is : " + node.getID());
        updateAllNodesFingerTables(node);
        updateAllFilesOnAllNodesExceptForDeleted();
        return node;
    }


    private Node createFingerTable(Node node) {
        if(!node.getFingerTable().isEmpty()){
            node.setFingerTable(new HashMap<>());
        }
        int k = node.getID();
        Node successor = null;
        for(int i = 0; i < 4; i++){
            int fingerID = (int) ((k + Math.pow(2, i)) % 16);
            if(nodeHashMap.containsKey(fingerID)){
                successor = nodeHashMap.get(fingerID);
            }else
            {
                int j = ( fingerID + 1 ) % 16;
                while( j != fingerID ){
                    if(listOfNodesOnline[j] != 0 && listOfNodesOnline[j] == 1){
                        successor = nodeHashMap.get(j);
                        break;
                    }
                    j = (j + 1) % 16;
                }
            }
            node.getFingerTable().put(fingerID, successor);
        }
        return node;
    }

    private void updateAllNodesFingerTables(Node currentNode) {
        Registry myReg;
        for (Node nodes : nodeHashMap.values()) {
            if (nodes.getID() != currentNode.getID()) {
                nodes.setFingerTable(createFingerTable(nodes).getFingerTable());
                try {
                    myReg = LocateRegistry.getRegistry(nodes.getIP(), 50086 + nodes.getID());
                    NodeInterface nodeInterface = (NodeInterface) myReg.lookup("node" + nodes.getID());
                    nodeInterface.updateFingerTable(nodes);
                } catch (RemoteException | NotBoundException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}
