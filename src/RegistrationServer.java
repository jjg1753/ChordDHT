
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;

public class RegistrationServer {
    private RegistrationServer() {
        super();
    }


    public static void main(String args[]) throws RemoteException {
            Registry reg = LocateRegistry.createRegistry(50086);//default rmi port
            ServerInterfaceImplementation s = new ServerInterfaceImplementation();
            reg.rebind("server", s); // giving name to the registry
            System.out.println("Server is running...");
    }
}
